package app;

import controller.FrameEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JFrame;
import models.RectangleDraw;
import models.iDraw;

/**
 *
 * @author pelletier, amady
 * 
 */
public class AcceuilFrame extends JFrame
{
    
    private static JButton btn1, btn2, btn3;
    private static GridLayout gL;
    
    public AcceuilFrame()
    {
        this.setLocation(new Point(0,0)); 
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(400,600));
        this.setTitle("Choissisez un type de fen�tre");
        gL = new GridLayout(3,0);
        setLayout(gL);
    }
    
    
    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater (new Runnable ( ) {
            @Override
            public void run()
            {
                AcceuilFrame af = new AcceuilFrame();
                // (Niveau d�butant) (Niveau interm�diaire) (Niveau professionnel)
                btn1 = new JButton("FEN 1");
                btn1.addActionListener(new FrameEvent()); 
                btn1.setForeground(Color.DARK_GRAY);
                btn1.setBackground(new Color(255,51,51));
                    
                btn2 = new JButton("FEN 2");
                btn2.setBackground(new Color(255,102,0));
                btn2.setForeground(Color.DARK_GRAY);
                btn2.addActionListener(new FrameEvent()); 
                
                btn3 = new JButton("FEN 3");
                btn3.setBackground(new Color(255,204,102));
                btn3.setForeground(Color.DARK_GRAY);
                btn3.addActionListener(new FrameEvent()); 
                
                af.add(btn1);
                af.add(btn2);
                af.add(btn3);
              
                af.pack();
                af.setVisible(true);
            }
        } ) ;
    }
}
