package models;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class ElipseDraw extends ShapeDraw
{

    public ElipseDraw(Color fond, Color bordure, Dimension dim)
    {
        super(fond, bordure, dim);	
    }

    @Override   
    public void draw(Graphics g)
    {
        g.setColor(fond);
        g.fillOval(rect.x,rect.y,rect.height,rect.width);
        g.setColor(bordure);
        g.drawOval(rect.x,rect.y,rect.height,rect.width); // en dur
    }
}

 