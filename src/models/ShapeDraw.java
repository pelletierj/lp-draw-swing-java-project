/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

public abstract class ShapeDraw implements iDraw
{
    protected Rectangle rect;
    protected Color fond;
    protected Color bordure;
    protected Point pos;
    protected int ecart = 0;
    
    /* Identifiant de la piece */
    protected static int inc = -1;
    protected final int iShape;

    public ShapeDraw(Color fond, Color bordure, Dimension dim)
    {
        //System.out.println("inc "+inc);
        inc++; // decrementation
	iShape = inc;
        this.fond = fond;
        this.bordure = bordure;
        this.pos = new Point(inc, inc);
        this.rect = new Rectangle(pos, dim);
    }
    
    public ShapeDraw()
    {
       System.out.println("ShapeDraw ()");
       this.fond = null;
       this.bordure = null;
       this.pos = new Point(0,0);
       this.rect = null;
       inc = -1;
       iShape = inc;
       System.out.println(" inc "+inc+" ishape "+iShape); 
    }
    

    @Override
    public void setProfondeur(Point p)
    {
        this.pos = p;
    }
    
    @Override
    public Point getProfondeur()
    {
        return this.pos;
    }
    
    @Override
    public abstract void draw(Graphics g);
	
    @Override
    public Rectangle getRectangle()
    {
        return (Rectangle) rect.clone();
    }
    
    @Override
    public int getId()
    {
        return this.iShape;
    }
}