package models;
import java.awt.Graphics;
//import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public interface iDraw
{
    public  void draw(Graphics g);
    public Rectangle getRectangle();
    
    public void setProfondeur(Point p);
    public Point getProfondeur();
    public int getId();
    //public void clear();
}