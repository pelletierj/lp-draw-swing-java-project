package models;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class RectangleDraw extends ShapeDraw
{
    
    public RectangleDraw(Color fond, Color bordure, Dimension dim)
    {
        super(fond, bordure, dim);	
    }

    public RectangleDraw()
    {
        super();	
    }
     
    @Override   
    public void draw(Graphics g)
    {
        g.setColor(fond);
        g.fillRect(rect.x,rect.y,rect.height,rect.width);
        g.setColor(bordure); // a mettre dans constructeur
        g.drawRect(rect.x,rect.y,rect.height,rect.width);
        
        /*
        Graphics2D g2d = (Graphics2D) g;
        Ellipse2D e = new Ellipse2D.Float(40, 40, 120, 120);
        GradientPaint gp = new GradientPaint(75, 75, Color.white,
            95, 95, Color.gray, true);
        g2d.setPaint(gp);
        g2d.fill(e);
        */
    }
}