/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.awt.BorderLayout;

import javax.accessibility.AccessibleContext;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

public class test {

  public static void main(final String args[])
  {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JDesktopPane desktop = new JDesktopPane();

    JInternalFrame iFrame = new JInternalFrame("Can Do All", true, true, true, true);
    JInternalFrame internalFrames[] = {
        iFrame,
        new JInternalFrame("Not Resizable", false, true, true, true),
        new JInternalFrame("Not Closable", true, false, true, true),
        new JInternalFrame("Not Maximizable", true, true, false, true),
        new JInternalFrame("Not Iconifiable", true, true, true, false) };

    int pos = 0;
    for (JInternalFrame internalFrame : internalFrames)
    {
      desktop.add(internalFrame);

      internalFrame.setBounds(pos * 25, pos * 25, 200, 100);
      pos++;

     
      JLabel label = new JLabel(internalFrame.getTitle(), JLabel.CENTER);
      internalFrame.add(label, BorderLayout.CENTER);

      internalFrame.setVisible(true);
    }
    desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
    
    desktop.setComponentZOrder(iFrame,1);
    
    
    frame.add(desktop, BorderLayout.CENTER);
    frame.setSize(500, 300);
    frame.setVisible(true);
  }
}