/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenuItem;

/**
 *
 * @author pelletier
 */
public class MenuEvent implements ActionListener{
    JFrame fen=null;
    
    public MenuEvent (JFrame f)
    {
        this.fen=f;
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
         if (e.getSource() instanceof JMenuItem )
         {
            JMenuItem jm = (JMenuItem) e.getSource();
            String item = jm.getText();
            
            //System.out.println(item);
            if("Nouveau".equals(item))
            {
                System.out.println("Nouveau");
            }
            if("X".equals(item))
            {
                System.out.println("X");
                this.fen.dispose();
            }
            if("Importer".equals(item))
            {
                //TODO
            }
            if("Exporter".equals(item))
            {
                //TODO
            }
        }
    }
}