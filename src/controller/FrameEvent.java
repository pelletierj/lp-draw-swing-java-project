package controller;

import views.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
//import app.AcceuilFrame;

/**
 *
 * @author pelletier
 */
public class FrameEvent implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() instanceof JButton )
        {
            Frame f1 = null;
            JButton jb = (JButton) e.getSource();
            
            if(jb.getText().equals("FEN 1"))
            {
                System.out.println("Ceci est la fen 1");
                f1 = new Frame("FEN 1");
                System.out.println("fen 1");
            }  
            else if(jb.getText().equals("FEN 2"))
            {
                System.out.println("Ceci est la fen 2");
                f1 = new Frame("FEN 2");
            }  
            else if(jb.getText().equals("FEN 3"))
            {
                System.out.println("Ceci est la fen 3");
                f1 = new Frame("FEN 3");
            }
            
            if(f1 != null)
            {
                f1.setVisible(true);
                f1.pack(); 
            }   
        }
    }
}
