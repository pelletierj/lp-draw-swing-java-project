package controller;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.event.MouseInputListener;
import models.ElipseDraw;
import models.RectangleDraw;
import models.iDraw;
import views.LeftPanel1;
import views.LeftPanel3;
import views.RightPanel;


public class ShapeDrawEvent implements MouseInputListener, ActionListener
{
    private RightPanel rp;
    private Point p = null;
    private iDraw shapeFocus;
    private int x, y;
    
    private iDraw idraw = null;
    public ShapeDrawEvent(RightPanel rp)
    {
        this.rp  = rp;
    }
    
    //1.2 (marche pas)
    @Override
    public void mouseDragged(MouseEvent e)
    {
        int x1 = e.getX();
        int y1 = e.getY();
        //System.out.println("mouseDragged x1"+x1); 
        
        //System.out.println("press form id : "+idraw.getId()); 
   
        if (idraw.getId() > 0)
        {
          rp.add(idraw);
          idraw.getRectangle().x = 100;
          idraw.getRectangle().y = 100;
          rp.add(idraw);
        }
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {}
       
     //1.1 (ok)
    @Override
    public void mouseClicked(MouseEvent e)
    {
    }
    
    //1.1 (ok)
    @Override
    public void mousePressed(MouseEvent e)
    {
       if (e.getSource() instanceof RightPanel)
        {
            this.x = e.getX();
            this.y = e.getY();
            rp.setShapeFocus(x, y);
            idraw  = rp.getShapeFocus();
            //System.out.println("mousePressed "+this.y+" - "+this.x);
        }
    }
    // 2.1
    @Override
    public void mouseReleased(MouseEvent e){}

    @Override
    public void mouseEntered(MouseEvent e){}

    @Override
    public void mouseExited(MouseEvent e){}

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() instanceof JButton)
        {
            JButton btn = (JButton) e.getSource();
            if(btn.getText().equals("+"))
            {
                if("FEN 1".equals(rp.getTitle()))
                {
                    draw("Rectangle");
                } 
                else if("FEN 2".equals(rp.getTitle()))
                {
                    draw("Elipse");
                } 
                else {
                    draw(LeftPanel3.getListItem());
                }
            } 
                   
            if(btn.getText().equals("-"))
            {
                shapeFocus = rp.getShapeFocus();
                if(shapeFocus != null)
                {
                    //System.out.println("remove shape id: "+shapeFocus.getId());
                    rp.remove(shapeFocus);
                    // methode de trie
                }
            }
            
            if(btn.getText().equals("Clear"))
            {
                //System.out.println("Clear click");
                this.rp.clear();
            }
        }  
    }
    
    public void draw(String shape)
    {
        iDraw shapeDraw = null;
        Color fond = LeftPanel1.getColor();
        Color bordure = Color.ORANGE; // a voir
        
        if (shape.equals("Rectangle"))
        {
            shapeDraw = new RectangleDraw(fond, bordure, new Dimension(100, 100));
            rp.add(shapeDraw);
        }  
        if (shape.equals("Elipse"))
        {
            shapeDraw = new ElipseDraw(fond, bordure, new Dimension(100, 100));
            rp.add(shapeDraw);
        }  
    }
}
