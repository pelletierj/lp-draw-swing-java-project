package views;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.MenuEvent;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JSeparator;

/**
 *
 * @author pelletier
 */
public class MenuBar extends JMenuBar{
    
    private static JMenuItem menu,menu1, menu2, menu3, menu4, menu5;
    
    public MenuBar(JFrame jf)
    {
        menu = new JMenuItem("Nouveau"); 
        menu.setBackground(Color.LIGHT_GRAY);
        menu.setForeground(Color.DARK_GRAY);
      
        menu1 = new JMenuItem("Exporter");
        menu1.setBackground(Color.LIGHT_GRAY);  
        menu1.setForeground(Color.WHITE);
        menu1.add(new JSeparator(JSeparator.VERTICAL));
        
        menu2 = new JMenuItem("Importer");
        menu2.setBackground(Color.LIGHT_GRAY);
        menu2.setForeground(Color.DARK_GRAY);
        menu2.add(new JSeparator(JSeparator.VERTICAL));
        
        menu3 = new JMenuItem("Admin");
        menu3.setBackground(Color.LIGHT_GRAY);
        menu3.setForeground(Color.RED);
        menu3.add(new JSeparator(JSeparator.VERTICAL));
        
        menu4 = new JMenuItem("X");
        menu4.setBackground(Color.LIGHT_GRAY);  
        menu4.setForeground(Color.DARK_GRAY);
        menu4.add(new JSeparator(JSeparator.VERTICAL));
        
        // menu.setOpaque(true);
       
        //Listener
        menu.addActionListener (new MenuEvent(jf));
        menu1.addActionListener (new MenuEvent(jf));
        menu2.addActionListener (new MenuEvent(jf));
        menu3.addActionListener (new MenuEvent(jf));
        menu4.addActionListener (new MenuEvent(jf));
       
        // ----------
        this.add(menu);
        this.add(menu1);
        this.add(menu2);
        this.add(menu3);
        this.add(menu4);
    }
}

