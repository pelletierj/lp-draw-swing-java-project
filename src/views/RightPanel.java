package views;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import models.RectangleDraw;
import models.iDraw;

/*
    Aide : http://www.java2s.com/Code/Java/Event/Mousedraganddraw.htm
*/
public class RightPanel extends JPanel 
{
    public Graphics2D g2 ;
    //public int x1;
    //public int y1;
    private List<iDraw> shapes = new ArrayList<iDraw>();
    private iDraw shapeFocus;
    private String title;
   
    public RightPanel(String title)
    {
        setBackground(Color.white);  
        this.title = title;
       // this.clear();
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paintComponent(g); //Important
        for(iDraw shape : shapes)
        {
            shape.draw(g);   
        }
    }
    
    // ok
    public iDraw getSquare(int x, int y)
    {
        int ind = 0;
        for(iDraw shape : shapes)
        {
            System.out.println("x : ("+ind+")"+shape.getRectangle().getX());
            System.out.println("y : ("+ind+")"+shape.getRectangle().getY());
            if(shape.getRectangle().contains(x, y))
            {
                System.out.println("getSquare Id : "+shape.getId());
                return shape; 
            }
            ind++;
        }
        return null;
    }
    
    
    public void setShapeFocus(int x, int y) 
    {
        for(iDraw shape : shapes)
        {
            if(shape.getRectangle().contains(x, y))
            {
                System.out.println("setShapeFocus : "+shape.getId());
                this.shapeFocus = shape;
            }
        } 
    }
    
    public iDraw getShapeFocus() 
    {
        return shapeFocus;
    }
     
    
    // ok
    public void add(iDraw shape)
    {
        shapes.add(shape);
        shapeFocus = shape;
        System.out.println("Add : "+shape.getId());
        repaint(); 
    }

    // ok
    public void remove(iDraw shape) //int indice
    {
      System.out.println("indice remove : "+shape.getId());
      //System.out.println("before : "+shapes.size());
      //this.shapes.remove(indice);

      this.shapes.remove(shape);
      repaint();
    }
    
    public List<iDraw> getList()
    {
        return this.shapes;
    }
    
    public void clear()
    {
        this.shapes.clear();
        this.shapeFocus = new RectangleDraw(); // réinitialise la classe abstraite
        repaint();    
    }
    
    
    public String getTitle()
    {
        return this.title;
    }
}
