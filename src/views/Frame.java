package views;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.ShapeDrawEvent;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Frame extends JFrame
{   
    private static RightPanel rf = null; // Action Panel
    private static JPanel p = null; // Palette graphique
    private static GridLayout gL;
    private static MenuBar mb;
    private static String ftype = "FEN 3"; // valeur par defaut
  
     
    public Frame(String FenType)
    {
        ftype = FenType; //
        this.setTitle(FenType);
        this.setLocation(new Point(0,0)); 
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(1260,660));
        
        gL = new GridLayout(0,2);
        setLayout(gL);
        
        mb = new MenuBar(this);
        this.setJMenuBar(mb);
        
        rf = new RightPanel(FenType);

        if("FEN 1".equals(ftype))
        {
            System.out.println("FEN (1):"+FenType);
            rf.addMouseListener(new ShapeDrawEvent(rf));
            p = new LeftPanel1(rf);  
        }
        else if("FEN 2".equals(ftype))
        {
            System.out.println("FEN (2):"+FenType);
            rf.addMouseListener(new ShapeDrawEvent(rf));
            p = new LeftPanel2(rf);  
        }
        else
        {
            System.out.println("FEN (3):"+FenType);
            rf.addMouseListener(new ShapeDrawEvent(rf));
            p = new LeftPanel3(rf);
        }

        if(p != null && rf != null)
        {
            this.add(p);
            this.add(rf);
        }
        this.pack();
        this.setVisible(true);
    }
    
    
   public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater (new Runnable ( ) {
            @Override
            public void run(){
                Frame f1 = new Frame(ftype+ftype); 
                System.out.println("dessine"); 
            }
        } ) ;
    }
}
