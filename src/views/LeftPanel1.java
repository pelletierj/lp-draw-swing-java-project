package views;
import controller.ShapeDrawEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class LeftPanel1 extends JPanel
{
    // Paneaux
    protected JPanel top = new JPanel();
    protected JPanel content = new JPanel();
    protected JPanel bootom = new JPanel();
    protected JPanel info = new JPanel();
     
    // Composants
    protected JLabel labforme = new JLabel("Elipse : ");
    //protected static JComboBox sel;
    protected static JButton btnAdd = new JButton("+");
    protected static JButton btnRemove = new JButton("-");
    
    private static JColorChooser graphicsPalette = new JColorChooser(Color.BLACK);
    private static JLabel labAdd = new JLabel("? : + Form -> Clickez dans la zone dessin");
    private static JButton btnClear = new JButton("Clear");
    
    public LeftPanel1(RightPanel rp)
    {
        setLayout(new BorderLayout());
        
        //Top : Liste formes
        /*String[] formes = {"", "Rectangle", "Elipse"};
        sel = new JComboBox(formes);
        sel.setSelectedIndex(0);*/
        top.setLayout(new FlowLayout());
        top.add(labforme);
        //top.add(sel);   
        top.add(btnAdd);
        top.add(btnRemove); 
        
        //top.add(new JLabel("okok"));
        btnAdd.addActionListener(new ShapeDrawEvent(rp));
        btnRemove.addActionListener(new ShapeDrawEvent(rp));
         
        // content : Coolor chooser
        graphicsPalette.setPreviewPanel(null);
        content.add(graphicsPalette);

        //Bootom : Info + Btn
        bootom.setLayout(new GridLayout(1,2));
        info.setLayout(new GridLayout(1,1));
        info.add(labAdd);
        bootom.add(info);
        
        // a supprimer (test)
        bootom.add(btnClear);
        btnClear.addActionListener(new ShapeDrawEvent(rp));
        setContentLayout();
    }

    public void setContentLayout()
    {
        add(top, BorderLayout.NORTH);
        add(content, BorderLayout.CENTER);
        add(bootom, BorderLayout.SOUTH);
        setVisible(true);
    }
    
    public static Color getColor()
    { 
        return graphicsPalette.getColor();
    }
}
