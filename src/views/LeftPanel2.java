package views;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class LeftPanel2 extends LeftPanel1
{

    protected JPanel top2;
    
    protected ButtonGroup group;
    
    protected JCheckBox radioColor = new JCheckBox("Couleur fond");
    protected JCheckBox radioBorderColor = new JCheckBox("Couleur bordure");

   // glassesButton = new JCheckBox("Glasses");
    

    
   // protected JRadioButton[] radioColor;
   
    public LeftPanel2(RightPanel rp)
    {
        super(rp);
        System.out.println("construct LeftPanel2");
        
        top2 = new JPanel();
        top2.setLayout(new FlowLayout());
        
        radioColor.setMnemonic(KeyEvent.VK_C); 
        radioColor.setSelected(true);
        radioBorderColor.setMnemonic(KeyEvent.VK_G); 
        radioBorderColor.setSelected(false);
       
        top2.add(new JLabel("Rectangle : "));
        top2.add(radioColor);
        top2.add(radioBorderColor);
        top2.add(btnAdd);
        top2.add(btnRemove);
        add(top2, BorderLayout.NORTH);
        
        System.out.println("left panel 2");
    }
}
