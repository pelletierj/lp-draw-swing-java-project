package views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LeftPanel3 extends LeftPanel2
{  
    protected JPanel top3;
    protected static JComboBox sel;
    
    protected static JTextField sizeX;
    protected static JTextField sizeY;
    
    
    public LeftPanel3(RightPanel rp)
    {
        super(rp);
        
        top3 = new JPanel();
        top3.setLayout(new FlowLayout());
        
        String[] formes = {"", "Rectangle", "Elipse"};
        sel = new JComboBox(formes);
        sel.setSelectedIndex(0);
        // top.setLayout(new FlowLayout());
        //top.add(labforme);
        top2.add(sel); 
        
        top2.add(radioColor);
        top2.add(radioBorderColor);

        sizeX = new JTextField();
        sizeX.setPreferredSize(new Dimension(35, 25));
        sizeY = new JTextField();
        sizeY.setPreferredSize(new Dimension(35, 25));
        
        top2.add(sizeX);
        top2.add(sizeY);
        
        top2.add(btnAdd);
        top2.add(btnRemove);
        
        System.out.println("left panel 3");
    }
    
    public static String getListItem()
    {
        return (String) sel.getSelectedItem();
    }
}
